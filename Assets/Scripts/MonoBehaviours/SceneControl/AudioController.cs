﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour
{
    private static AudioController instance;
    public static AudioController Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<AudioController>();
            }

            return instance;
        }
    }

    public AudioSource music;
    public AudioSource effects;
    public AudioSource ambient;
    public AudioSource voice;
}
