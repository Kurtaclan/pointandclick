using System;

public class LostItemReaction : DelayedReaction
{
    public string itemName;

    protected override void SpecificInit()
    {

    }

    protected override void ImmediateReaction()
    {
        Inventory.Instance.DisplayInventoryGrid(true);
        Inventory.Instance.RemoveItem(itemName);        
    }

    public override void End()
    {
        //Nothing to see here
    }
}
