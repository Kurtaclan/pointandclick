﻿using UnityEngine;
using System.Linq;
using System;

public class DialogTextReaction : Reaction
{
    public DialogNode dialogNode;
    public TextMesh dialogMesh;
    public Transform dialogCameraPosition;

    protected override void SpecificInit()
    {
        
    }

    protected override void ImmediateReaction()
    {
        DialogManager.Instance.DisplayMessage(dialogNode, dialogMesh, dialogCameraPosition);
    }

    public override void End()
    {
        DialogManager.Instance.CloseDialog();
    }
}