﻿using UnityEngine;
using UnityEngine.UI;

public class GridSlot : MonoBehaviour
{
    public Item slotItem;

    private void Awake()
    {
        slotItem = GetComponentInChildren<Item>();
    }

    public virtual void SetItem(Item item, bool destroy = false)
    {
        if(destroy)
        {
            Destroy(slotItem.gameObject);
        }

        slotItem = item;
    }

    public virtual void AddItem(Item item)
    {
        slotItem = Instantiate(item, transform);
        Destroy(item.transform.parent.gameObject);
    }
}
