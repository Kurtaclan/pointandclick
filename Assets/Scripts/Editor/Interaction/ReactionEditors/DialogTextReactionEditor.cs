using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DialogTextReaction))]
public class DialogTextReactionEditor : ReactionEditor
{
    private SerializedProperty dialogNodeProperty;
    private const string dialogNodeName = "dialogNode";

    private SerializedProperty dialogMeshProperty;
    private const string dialogMeshName = "dialogMesh";

    private SerializedProperty dialogCameraPositionProperty;
    private const string dialogCameraPositionName = "dialogCameraPosition";
    
    protected override void Init()
    {
        dialogNodeProperty = serializedObject.FindProperty(dialogNodeName);
        dialogMeshProperty = serializedObject.FindProperty(dialogMeshName);
        dialogCameraPositionProperty = serializedObject.FindProperty(dialogCameraPositionName);
    }

    protected override void DrawReaction()
    {
        EditorGUILayout.PropertyField(dialogNodeProperty);
        EditorGUILayout.PropertyField(dialogMeshProperty);
        EditorGUILayout.PropertyField(dialogCameraPositionProperty);
    }

    protected override string GetFoldoutLabel()
    {
        return "Dialog Text Reaction";
    }
}
