﻿using UnityEngine;
using System.Linq;
using System;

public class TerminalReaction : Reaction
{
    public TerminalNode[] matrix;
    public int columnCount;
    public ComputerTerminal terminal;
    public Condition condition;
    public int terminalNumber;
    public ReactionCollection followUpCollection;

    protected override void SpecificInit()
    {

    }

    protected override void ImmediateReaction()
    {
        TerminalManager.Instance.DisplayTerminal(this);
    }

    public override void End()
    {
        TerminalManager.Instance.CloseTerminal();
    }
}