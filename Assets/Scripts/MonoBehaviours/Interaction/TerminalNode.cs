﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[System.Serializable]
public class TerminalNode
{
    public Color Color;
    public TerminalNodeDirection Direction;
}

public enum TerminalNodeDirection
{
    left,
    up,
    right,
    down
}