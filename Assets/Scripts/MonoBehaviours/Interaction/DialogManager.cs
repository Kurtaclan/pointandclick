using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using System.Collections;

public class DialogManager : MonoBehaviour
{
    public GameObject dialogContainer;
    public GameObject textContainer;
    public DialogNode closeNode;
    private DialogText[] dialogTexts = new DialogText[0];
    private TextMesh dialogMesh;
    public CanvasGroup faderCanvasGroup;
    private static DialogManager instance;

    public static DialogManager Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<DialogManager>();
            }

            return instance;
        }
    }

    private void Start()
    {
        dialogContainer.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (dialogTexts.Length == 0)
        {
            dialogTexts = GetComponentsInChildren<DialogText>(true).OrderBy(x => x.name).ToArray();
        }

        if (dialogMesh)
        {
            dialogMesh.transform.rotation = Quaternion.LookRotation(dialogMesh.transform.position - Camera.main.transform.position);
        }
    }

    public void DisplayMessage(DialogNode node, TextMesh textMesh, Transform cameraPosition = null)
    {
        MouseController.Instance.OverrideMouse_Dialog = true;
        MissionController.Instance.ShowHide(false);
        //Inventory.Instance.DisplayInventoryGrid(false);
        PlayerMovement.Instance.StopMovement();
        textContainer.gameObject.SetActive(false);
        dialogMesh = textMesh;

        if (node.dialogText == "Close")
        {
            CloseDialog();
            return;
        }

        ClearDialog();

        if (cameraPosition)
        {
            CameraControl.Instance.CinematicCamMode(true, faderCanvasGroup, cameraPosition);
        }

        PlayerMovement.Instance.dialogMesh.text = WrapText(string.IsNullOrEmpty(node.dialogText) ? "" : node.dialogText);
        PlayerMovement.Instance.dialogMesh.color = node.dialogColor;

        StartCoroutine(IteractionResponse(node.responseText, node.dialogColor, textMesh, node));
    }

    private IEnumerator IteractionResponse(string text, Color color, TextMesh textMesh, DialogNode node)
    {
        yield return new WaitForSeconds(1.3f);

        textMesh.text = WrapText(string.IsNullOrEmpty(text) ? "" : text); ;
        textMesh.color = color;

        /** Option to split long dialog up into shorter parts
        string dialogText = "";
        while (HasDialog(ref text, out dialogText))
        {
            textMesh.text = dialogText;
            textMesh.color = color;

            yield return new WaitForSeconds(1.5f);
        }
        **/
        yield return new WaitForSeconds(0.8f);
        ShowResponses(node, textMesh);
    }

    private bool HasDialog(ref string text, out string dialogText)
    {
        if (text.Length > 0)
        {
            var stringLength = (text.Length >= 30 ? 30 : text.Length);

            while (stringLength != text.Length && char.IsPunctuation(text[(stringLength)]))
            {
                stringLength++;
            }

            dialogText = text.Substring(0, stringLength);
            text = text.Remove(0, stringLength);

            return true;
        }

        dialogText = "";
        return false;
    }

    private string WrapText(string input)
    {
        // Split string by char " "         
        string[] words = input.Split(" "[0]);

        // Prepare result
        string result = "";

        // Temp line string
        string line = "";

        // for each all words        
        foreach (string s in words)
        {
            // Append current word into line
            string temp = line + " " + s;

            // If line length is bigger than lineLength
            if (temp.Length > 29)
            {

                // Append current line into result
                result += line + "\n";
                // Remain word append into new line
                line = s;
            }
            // Append current word into current line
            else
            {
                line = temp;
            }
        }

        // Append last line into result        
        result += line;

        // Remove first " " char
        return result.Substring(1, result.Length - 1);
    }

    private void ShowResponses(DialogNode node, TextMesh textMesh)
    {
        dialogContainer.gameObject.SetActive(true);

        for (int i = 0; i < dialogTexts.Length; i++)
        {
            if (node.nodes.Length <= i)
            {
                dialogTexts[i].SetNode(null, textMesh);
                continue;
            }

            dialogTexts[i].SetNode(node.nodes[i], textMesh);
        }
    }

    public void CloseDialog()
    {
        MouseController.Instance.OverrideMouse_Dialog = false;
        PlayerMovement.Instance.AllowMove();
        StopAllCoroutines();
        dialogContainer.gameObject.SetActive(false);
        PlayerMovement.Instance.dialogMesh.text = "";

        if (dialogMesh)
        {
            dialogMesh.text = "";
        }

        CameraControl.Instance.CinematicCamMode(false, faderCanvasGroup);
    }

    private void ClearDialog()
    {
        PlayerMovement.Instance.dialogMesh.text = "";

        foreach (var a in dialogTexts)
        {
            a.ClearNode();
        }
    }
}

