﻿using System;
using UnityEngine;

public class PlayerStopReaction : DelayedReaction
{
    public float stopDuration;
    protected override void ImmediateReaction()
    {
        PlayerMovement.Instance.StopMovement(stopDuration);
    }

    public override void End()
    {
        
    }
}