﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapInfo : MonoBehaviour
{
    public MeshCollider Floor;

    public Vector3[] GetBounds()
    {
        Vector3[] temp = new Vector3[4];
        //TopRight
        temp[0] = Floor.bounds.center + new Vector3(Floor.bounds.size.x * 0.4f, Floor.bounds.size.y * 0.5f, Floor.bounds.size.z * 0.2f);
        //BottomRight
        temp[1] = Floor.bounds.center + new Vector3(Floor.bounds.size.x * 0.4f, Floor.bounds.size.y * 0.5f, -Floor.bounds.size.z * 0.6f);
        //BottomLeft
        temp[2] = Floor.bounds.center + new Vector3(-Floor.bounds.size.x * 0.4f, Floor.bounds.size.y * 0.5f, -Floor.bounds.size.z * 0.6f);
        //TopLeft
        temp[3] = Floor.bounds.center + new Vector3(-Floor.bounds.size.x * 0.4f, Floor.bounds.size.y * 0.5f, Floor.bounds.size.z * 0.2f);
        return temp;
    }
}
