﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;

[CustomEditor(typeof(DialogNode))]
public class DialogNodeEditor : EditorWithSubEditors<DialogNodeEditor, DialogNode>
{
    public bool showNode;
    private DialogNode dialogNode;
    private SerializedProperty dialogTextProperty;
    private SerializedProperty dialogColorProperty;
    private SerializedProperty dialogNodeProperty;
    private SerializedProperty responseTextProperty;

    private const float collectionButtonWidth = 125f;
    private const string dialogTextName = "dialogText";
    private const string dialogColorName = "responseText";
    private const string dialogNodeName = "nodes";
    private const string responseTextName = "dialogColor";

    private void OnEnable()
    {
        if (target == null)
        {
            return;
        }

        dialogNode = (DialogNode)target;

        dialogTextProperty = serializedObject.FindProperty(dialogTextName);
        dialogColorProperty = serializedObject.FindProperty(dialogColorName);
        dialogNodeProperty = serializedObject.FindProperty(dialogNodeName);
        responseTextProperty = serializedObject.FindProperty(responseTextName);

        List<DialogNode> nodeList = new List<DialogNode>();
        foreach (Transform child in dialogNode.transform)
        {
            var temp = child.GetComponent<DialogNode>();

            if (temp != null)
            {
                nodeList.Add(temp);
            }
        }

        dialogNode.nodes = nodeList.ToArray();

        CheckAndCreateSubEditors(ref dialogNode.nodes);
    }

    protected override void SubEditorSetup(DialogNodeEditor editor)
    {
        //nothing needs to happen here
    }

    public override void OnInspectorGUI()
    {
        if (serializedObject.targetObject == null)
        {
            return;
        }

        serializedObject.Update();

        CheckAndCreateSubEditors(ref dialogNode.nodes);

        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(dialogTextProperty);
        EditorGUILayout.PropertyField(dialogColorProperty);
        EditorGUILayout.PropertyField(responseTextProperty);

        EditorGUILayout.Space();
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        if (GUILayout.Button("Add Child Node", GUILayout.Width(collectionButtonWidth)))
        {
            dialogNodeProperty.AddToObjectArray(CreateDialogNode());
        }

        if (dialogNode.nodes.Length > 0)
        {
            if (GUILayout.Button("Remove Child Node", GUILayout.Width(collectionButtonWidth)))
            {
                var nodeToRemove = dialogNode.nodes[dialogNode.nodes.Length - 1];
                dialogNodeProperty.RemoveFromObjectArray(nodeToRemove);
                DestroyImmediate(nodeToRemove.gameObject);
            }
        }

        EditorGUILayout.EndHorizontal();

        if (dialogNode.nodes.Length > 0)
        {
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUI.indentLevel++;
            showNode = EditorGUILayout.Foldout(showNode, "Nodes");

            if (showNode)
            {
                ExpandUI();
            }
            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();
        }

        EditorGUILayout.EndVertical();
        serializedObject.ApplyModifiedProperties();
    }

    public void ExpandUI()
    {
        EditorGUI.indentLevel++;
        for (int i = 0; i < subEditors.Length; i++)
        {
            subEditors[i].OnInspectorGUI();
            EditorGUILayout.Space();
        }
        EditorGUI.indentLevel--;
    }

    public DialogNode CreateDialogNode()
    {
        GameObject obj = new GameObject("Dialog Node", typeof(DialogNode));        
        obj.transform.parent = dialogNode.transform;
        obj.transform.localPosition = new Vector3(0, 0, 0);
        return obj.GetComponent<DialogNode>();
    }
}
