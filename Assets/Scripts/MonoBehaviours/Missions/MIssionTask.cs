﻿using System;
using UnityEngine;

[Serializable]
public class MissionTask
{
    [SerializeField]
    private string _taskTitle;

    public string TaskTitle
    {
        get { return _taskTitle; }
        set
        {
            if (_taskTitle == value) return;
            _taskTitle = value;
        }
    }


    [SerializeField]
    private bool _taskComplete;

    public bool TaskComplete
    {
        get { return _taskComplete; }
        set
        {
            if (_taskComplete == value) return;
            _taskComplete = value;
        }
    }
}