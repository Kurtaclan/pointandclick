﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class MissionMono : MonoBehaviour
{
    Mission mission;
    public MissionTaskMono MissionTaskPrefab;
    public Text missionTitle;
    public GameObject missionTaskList;
    bool complete = false;
    bool collapsed = false;

    public void Created(Mission mission)
    {
        this.mission = mission;
        missionTitle.text = this.mission.MissionTitle;

        foreach (var task in mission.MissionTasks)
        {
            var missionTask = Instantiate(MissionTaskPrefab, missionTaskList.transform);
            missionTask.Created(task);
        }
    }

    public void Collapse()
    {
        collapsed = !collapsed;

        if (collapsed)
        {
            missionTaskList.SetActive(false);
        }
        else
        {
            missionTaskList.SetActive(true);
        }

        /*
        This fixes a UI bug that unity doesn't care about fixing, hurray hacky solutions that shouldn't work.
        Basically the content sizer gets confused if sibling elements are enabled and disabled in a small window of time.
        To fix this we create a GameObject with an Image component so that the content sizer redraws, 
        calling this manually doesn't work but creating a new layout component does.
        */

        #region hacky ui fix
        var temp = new GameObject("Hacky UI Fix");
        temp.transform.SetParent(transform);
        temp.AddComponent<Image>();
        Destroy(temp);
        #endregion
    }

    public void Update()
    {
        if (mission == null)
        {
            return;
        }

        if (mission.MissionComplete != complete)
        {
            complete = mission.MissionComplete;
            StartCoroutine(MarkMissionComplete());
        }
    }

    private IEnumerator MarkMissionComplete()
    {
        yield return new WaitForSeconds(3f);
        gameObject.SetActive(false);
    }
}
