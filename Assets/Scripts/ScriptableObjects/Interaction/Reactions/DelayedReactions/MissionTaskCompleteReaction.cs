﻿using System;
using UnityEngine;

public class MissionTaskCompleteReaction : DelayedReaction
{
    public string MissionTaskName;

    protected override void ImmediateReaction()
    {
        MissionController.Instance.ShowHide(true);
        MissionController.Instance.MissionTaskComplete(MissionTaskName);
    }

    public override void End()
    {
    }
}