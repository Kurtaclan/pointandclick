﻿using UnityEditor;

[CustomEditor(typeof(BroadcastReaction))]
public class BroadcastReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Broadcast Reaction";
    }
}
