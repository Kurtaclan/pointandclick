﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CubePuzzle : Interactable
{
    public CubePuzzleChild[] Children;
    private Rigidbody rigid_body;
    public float pushspeed = 7.5f;
    public bool hasPushed = false;

    private void Start()
    {
        Children = GetComponentsInChildren<CubePuzzleChild>();
        rigid_body = GetComponent<Rigidbody>();
    }

    internal override IEnumerator interact()
    {
        if (Children.Length == 1 && PlayerMovement.Instance.Box)
        {
            TextManager.Instance.DisplayMessage("I don't think I can reach that high.", Color.white, 0);
        }
        else if (Children.Length == 1)
        {
            //pick up
            PlayerMovement.Instance.Box = Children[0].gameObject;
            Children[0].transform.SetParent(PlayerMovement.Instance.transform);
            Children = GetComponentsInChildren<CubePuzzleChild>();
        }
        else if (PlayerMovement.Instance.Box)
        {
            //set down
            PlayerMovement.Instance.Box.transform.SetParent(transform);
            PlayerMovement.Instance.Box.transform.localPosition = new Vector3(0, 2, 0);
            PlayerMovement.Instance.Box.transform.localEulerAngles = new Vector3(0, Random.Range(-8f, 8f), 0);
            PlayerMovement.Instance.Box = null;
            Children = GetComponentsInChildren<CubePuzzleChild>();
        }
        else if (!hasPushed)
        {
            //push
            hasPushed = true;
            rigid_body.velocity = PlayerMovement.Instance.transform.forward * pushspeed;
        }

        yield return null;
    }
}
