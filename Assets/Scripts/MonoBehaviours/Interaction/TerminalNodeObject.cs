﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class TerminalNodeObject : MonoBehaviour
{
    public Image image;
    public Image line;
    public Image lineChild;
    public bool lineEnabled
    {
        get { return lineChild.gameObject.activeSelf; }
        set
        {
            if (lineChild)
            {
                lineChild.gameObject.SetActive(value);
            }
        }
    }
    public TerminalNodeDirection direction;

    public void SetupNode(TerminalNode node)
    {
        image.color = node.Color;
        lineChild.color = node.Color;
        direction = node.Direction;
        transform.localEulerAngles = DirectionToRotation(direction);

        line.transform.SetParent(TerminalManager.Instance.lineContainer);
    }

    private Vector3 DirectionToRotation(TerminalNodeDirection direction)
    {
        switch (direction)
        {
            case TerminalNodeDirection.down:
                return new Vector3(0, 0, 270);
            case TerminalNodeDirection.left:
                return new Vector3(0, 0, 180);
            case TerminalNodeDirection.right:
                return new Vector3(0, 0, 0);
            default:
                return new Vector3(0, 0, 90);
        }
    }

    public void RotateNode()
    {
        switch (direction)
        {
            case TerminalNodeDirection.down:
                direction = TerminalNodeDirection.left;
                break;
            default:
                direction = direction + 1;
                break;
        }

        line.transform.localEulerAngles = transform.localEulerAngles = DirectionToRotation(direction);
    }

    public void NodeClick()
    {
        TerminalManager.Instance.RotateNodes(image.color);
    }
}