﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.AI;
using System;

public class PlayerMovement : MonoBehaviour
{
    public Animator animator;
    public NavMeshAgent agent;
    public TextMesh dialogMesh;
    public PlayerState playerState;
    public float TurnSpeedThreshold = 0.5f;
    public float speedDampTime = 0.1f;
    public float slowingSpeed = 0.175f;
    public float turningSpeed = 15f;
    public SaveData playerSaveData;
    private Interactable currentInteractable;
    private Interactable previousInteractable;
    private bool HandleInput = true;
    private bool IsFrozen = false;
    public GameObject Box;
    private readonly int hashSpeedTag = Animator.StringToHash("Speed");
    public const string startingPositionKey = "starting position";
    private const float navMeshSampleDistance = 4f;
    public Vector3 TargetPosition;
    private bool _offMeshNav = false;
    private Vector3 _offMeshNavPos;

    private static PlayerMovement instance;
    public static PlayerMovement Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<PlayerMovement>();
            }

            return instance;
        }
    }    

    private void Awake()
    {
        agent.updateRotation = false;
        string startingPoint = "";
        playerSaveData.Load(startingPositionKey, ref startingPoint);
        try
        {
            Transform start = GameObject.Find(startingPoint).transform;
            transform.position = TargetPosition = start.position;
            transform.rotation = start.rotation;
        }
#pragma warning disable
        catch (Exception ex)
        {
            //position is wack yo
        }
#pragma warning restore
    }

    private void OnAnimatorMove()
    {
        if (Time.deltaTime == 0) { return; }

        agent.velocity = animator.deltaPosition / Time.deltaTime;
    }

    private void Update()
    {
        float speed = agent.desiredVelocity.magnitude;

        if (agent.isOnOffMeshLink)
        {
            _offMeshNav = true;
            _offMeshNavPos = agent.currentOffMeshLinkData.endPos;
        }

        if (_offMeshNav)
        {
            agent.enabled = false;

            speed = 2;
            transform.position = Vector3.MoveTowards(transform.position, _offMeshNavPos, Time.deltaTime * 1.5f);

            Vector3 relativePos = _offMeshNavPos - transform.position;
            Quaternion targetRotation = Quaternion.LookRotation(relativePos);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, turningSpeed * Time.deltaTime);

            if (playerState != PlayerState.Moving)
            {
                playerState = PlayerState.Moving;
            }

            if (Vector3.Distance(transform.position, _offMeshNavPos) < 0.1f)
            {
                _offMeshNav = false;
                agent.enabled = true;
                agent.CompleteOffMeshLink();

                if(currentInteractable != null)
                {
                    Interact(currentInteractable);
                }
            }
        }
        else if (IsMoving(speed))
        {
            Quaternion targetRotation = Quaternion.LookRotation(agent.desiredVelocity);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, turningSpeed * Time.deltaTime);

            if (playerState != PlayerState.Moving)
            {
                playerState = PlayerState.Moving;
            }
        }
        else if (IsTurning())
        {
            speed = 0f;

            if (!agent.isStopped)
            {
                agent.isStopped = true;
                playerState = PlayerState.Turning;
            }

            if (Mathf.Abs(Mathf.Abs(transform.rotation.y) - Mathf.Abs(currentInteractable.interactionLocation.rotation.y)) > 0.02f)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, currentInteractable.interactionLocation.rotation, 5 * Time.deltaTime);
            }
            else
            {
                transform.rotation = currentInteractable.interactionLocation.rotation;
            }
        }
        else if (IsInteracting())
        {
            CurrentInteractableInteract();

            if (playerState != PlayerState.Stationary)
            {
                playerState = PlayerState.Stationary;
            }
        }
        else
        {
            playerState = PlayerState.Stationary;
        }

        animator.SetFloat(hashSpeedTag, speed, speedDampTime, Time.deltaTime);
    }

    private bool IsMoving(float speed)
    {
        return speed > TurnSpeedThreshold;
    }

    private bool IsTurning()
    {
        if (currentInteractable != null && transform.rotation == currentInteractable.interactionLocation.rotation && playerState == PlayerState.Moving)
        {
            playerState = PlayerState.Turning;
        }

        return (playerState == PlayerState.Moving || playerState == PlayerState.Turning) && currentInteractable != null && transform.rotation != currentInteractable.interactionLocation.rotation;
    }

    private bool IsInteracting()
    {
        if (previousInteractable != null)
        {
            return false;
        }

        return playerState == PlayerState.Turning && currentInteractable != null && !currentInteractable.Interacting;
    }

    internal void AllowMove()
    {
        IsFrozen = false;
        HandleInput = true;
    }

    public void Interact(Interactable interactable, bool isFrozen = false, bool handleInput = true)
    {
        IsFrozen = isFrozen;
        HandleInput = handleInput;

        Interact(interactable);
    }

    private void Interact(Interactable interactable)
    {
        agent.enabled = true;
        currentInteractable = interactable;
        currentInteractable.BeginInteract();

        if (transform.rotation != currentInteractable.interactionLocation.rotation && Vector3.Distance(transform.position, currentInteractable.interactionLocation.position) <= 0.3f)
        {
            playerState = PlayerState.Turning;
        }
        else if (transform.rotation != currentInteractable.interactionLocation.rotation || Vector3.Distance(transform.position, currentInteractable.interactionLocation.position) > 0.3f)
        {
            //We need to move to the Interaction Location before we can Interact.
            agent.SetDestination(currentInteractable.interactionLocation.position);
            agent.isStopped = false;
            TargetPosition = agent.destination;
        }
        else
        {
            //We are already at the Interaction Location, so Interact with it.
            CurrentInteractableInteract();
        }
    }

    internal void StopMovement()
    {
        IsFrozen = true;
        HandleInput = false;
        agent.isStopped = true;
    }

    internal void StopMovement(float stopDuration)
    {
        StopMovement();

         StartCoroutine(stopMovement(stopDuration));
    }

    private IEnumerator stopMovement(float stopDuration)
    {
        yield return new WaitForSeconds(stopDuration);
        AllowMove();
    }

    private void CurrentInteractableInteract()
    {
        if (currentInteractable.Interact())
        {
            previousInteractable = currentInteractable;
            currentInteractable = null;
        }
    }

    public void OnInteractableClick(Interactable interactable)
    {
        if (!HandleInput || IsFrozen || (interactable is SeeingInteractable))
        {
            return;
        }

        MouseController.Instance.AnimateMove();
        if (!ClearInteraction())
        {
            return;
        }

        Interact(interactable);
    }

    public void OnGroundClick(BaseEventData data)
    {
        if (!HandleInput || IsFrozen)
        {
            return;
        }

        MouseController.Instance.AnimateMove();
        if(!ClearInteraction())
        {
            return;
        }

        agent.enabled = true;

        PointerEventData pData = (PointerEventData)data;
        NavMeshHit hit;

        if (NavMesh.SamplePosition(pData.pointerCurrentRaycast.worldPosition, out hit, navMeshSampleDistance, NavMesh.AllAreas))
        {
            agent.SetDestination(hit.position);
        }
        else
        {
            agent.SetDestination(pData.pointerCurrentRaycast.worldPosition);
        }

        TargetPosition = agent.destination;
        agent.isStopped = false;
    }

    private bool ClearInteraction()
    {
        if (previousInteractable != null)
        {
            if (previousInteractable.EndInteraction())
            {
                previousInteractable = null;
            }
            else
            {
                return false;
            }
        }

        currentInteractable = null;

        if (!animator.GetCurrentAnimatorStateInfo(0).IsTag("Locomotion") && !animator.GetCurrentAnimatorStateInfo(0).IsTag("Idle"))
        {
            animator.SetTrigger("Idle");
        }

        return true;
    }
}

public enum PlayerState
{
    Stationary,
    Moving,
    Turning,
    Interacting
}