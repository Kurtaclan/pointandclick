using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MovingInteractable))]
public class MovingInteractableEditor : EditorWithSubEditors<ConditionCollectionEditor, ConditionCollection>
{
    private MovingInteractable interactable;
    private SerializedProperty interactionLocationProperty;
    private SerializedProperty interactionAgentProperty;
    private SerializedProperty interactionNodeContainerProperty;
    private SerializedProperty collectionsProperty;
    private SerializedProperty defaultReactionCollectionProperty;

    private const float collectionButtonWidth = 150f;
    private const string interactablePropInteractionLocationName = "interactionLocation";
    private const string interactablePropInteractionAgentName = "agent";
    private const string interactablePropInteractionNodeContainerName = "nodeContainer";
    private const string interactablePropConditionCollectionsName = "conditionCollections";
    private const string interactablePropDefaultReactionCollectionName = "defaultReactionCollection";    

    private void OnEnable ()
    {
        interactable = (MovingInteractable)target;

        collectionsProperty = serializedObject.FindProperty(interactablePropConditionCollectionsName);
        interactionLocationProperty = serializedObject.FindProperty(interactablePropInteractionLocationName);
        interactionAgentProperty = serializedObject.FindProperty(interactablePropInteractionAgentName);
        interactionNodeContainerProperty = serializedObject.FindProperty(interactablePropInteractionNodeContainerName);
        defaultReactionCollectionProperty = serializedObject.FindProperty(interactablePropDefaultReactionCollectionName);
        
        CheckAndCreateSubEditors(ref interactable.conditionCollections);
    }

    private void OnDisable ()
    {
        CleanupEditors ();
    }

    protected override void SubEditorSetup(ConditionCollectionEditor editor)
    {
        editor.collectionsProperty = collectionsProperty;
    }

    public override void OnInspectorGUI ()
    {
        serializedObject.Update ();
        
        CheckAndCreateSubEditors(ref interactable.conditionCollections);
        
        EditorGUILayout.PropertyField (interactionLocationProperty);
        EditorGUILayout.PropertyField(interactionAgentProperty);
        EditorGUILayout.PropertyField(interactionNodeContainerProperty);

        for (int i = 0; i < subEditors.Length; i++)
        {
            subEditors[i].OnInspectorGUI ();
            EditorGUILayout.Space ();
        }

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace ();
        if (GUILayout.Button("Add Condition Collection", GUILayout.Width(collectionButtonWidth)))
        {
            ConditionCollection newCollection = ConditionCollectionEditor.CreateConditionCollection ();
            collectionsProperty.AddToObjectArray (newCollection);
        }
        EditorGUILayout.EndHorizontal ();

        EditorGUILayout.Space ();

        EditorGUILayout.PropertyField (defaultReactionCollectionProperty);

        serializedObject.ApplyModifiedProperties ();
    }
}
