﻿using System.Collections;
using UnityEngine;

public abstract class HoldReaction : Reaction
{
    public bool ReactionComplete = false;

    public new void Init()
    {
        SpecificInit();
    }

    public IEnumerator HoldReact(MonoBehaviour monoBehaviour)
    {
        ImmediateReaction();

        return null;
    }
}
