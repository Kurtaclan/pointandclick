using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using System.Collections;

public class TerminalManager : MonoBehaviour
{
    public GameObject terminalContainer;
    public GameObject terminalBackground;
    public CanvasGroup faderCanvasGroup;
    public TerminalNodeObject NodePrefab;
    public RectTransform startImage;
    public RectTransform endImage;
    public Transform lineContainer;

    private Condition condition;
    private ReactionCollection followUpCollection;
    private static TerminalManager instance;
    private float imageWidth = 113.4f;
    private RectTransform terminalRect;
    private RectTransform lineRect;
    private TerminalNodeObject[] nodes;
    private int columnCount;
    private int rowCount;
    private ComputerTerminal currentTerminal;
    private int currentTerminalNumber;

    public static TerminalManager Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<TerminalManager>();
            }

            return instance;
        }
    }

    private void Start()
    {
        terminalRect = terminalContainer.GetComponent<RectTransform>();
        lineRect = lineContainer.GetComponent<RectTransform>();
        terminalBackground.gameObject.SetActive(false);
    }

    public void RotateNodes(Color color)
    {
        var nodesToRotate = nodes.Where(x => x.image.color == color);

        foreach (var node in nodesToRotate)
        {
            node.RotateNode();
        }

        CalculateRoute();

        //Do it twice because I'm lazy
        CalculateRoute();
    }

    private void CalculateRoute()
    {
        for (int j = 0; j < nodes.Length - 1; j += columnCount)
            for (int i = 0; i < columnCount; i++)
            {
                if (i == 0 && j == 0)
                {
                    nodes[i + j].lineEnabled = nodes[i + j].direction != TerminalNodeDirection.left && nodes[i + j].direction != TerminalNodeDirection.up;
                }
                else if (nodes[i + j] == nodes.Last())
                {
                    nodes[i + j].lineEnabled = nodes[i + j].direction == TerminalNodeDirection.right && CheckNodeNeighbors(i, j);

                    if (nodes[i + j].lineEnabled)
                    {
                        //WE DONE BOYS                       

                        if (condition)
                        {
                            AllConditions.Instance.conditions.FirstOrDefault(x => x.hash == condition.hash).satisfied = true;                            
                        }

                        if (currentTerminal)
                        {
                            currentTerminal.EnableTerminal(currentTerminalNumber);
                        }

                        TextManager.Instance.DisplayMessage("Terminal Complete", Color.black, 0f);
                        StartCoroutine(CloseTerminal());
                    }
                }
                else if (NodeBorder(i, j))
                {
                    nodes[i + j].lineEnabled = false;
                }
                else
                {
                    nodes[i + j].lineEnabled = CheckNodeNeighbors(i, j);
                }
            }
    }

    private IEnumerator FollowUpReaction()
    {
        yield return null;

        foreach (var a in followUpCollection.reactions)
        {
            if (a is DelayedReaction)
            {
                yield return ((DelayedReaction)a).wait;
            }

            followUpCollection.ReactNext();
        }
    }

    private bool NodeBorder(int i, int j)
    {
        return (i == 0 && nodes[i + j].direction == TerminalNodeDirection.left) || (i == columnCount - 1 && nodes[i + j].direction == TerminalNodeDirection.right)
            || (j == 0 && nodes[i + j].direction == TerminalNodeDirection.up) || (j == nodes.Length - 1 && nodes[i + j].direction == TerminalNodeDirection.down);
    }

    private bool CheckNodeNeighbors(int i, int j)
    {
        if (NodeInBounds(i + j + 1)
            && (nodes[i + j + 1].lineEnabled
            && nodes[i + j + 1].direction == TerminalNodeDirection.left 
            && nodes[i + j].direction != TerminalNodeDirection.right)

            || (NodeInBounds(i + j - 1)
            && (nodes[i + j - 1].lineEnabled
            && nodes[i + j - 1].direction == TerminalNodeDirection.right 
            && nodes[i+j].direction != TerminalNodeDirection.left))

            || (NodeInBounds(i + j - columnCount)
            && (nodes[i + j - columnCount].lineEnabled
            && nodes[i + j - columnCount].direction == TerminalNodeDirection.down 
            && nodes[i + j].direction != TerminalNodeDirection.up))

            || (NodeInBounds(i + j + columnCount)
            && (nodes[i + j + columnCount].lineEnabled
            && nodes[i + j + columnCount].direction == TerminalNodeDirection.up 
            && nodes[i + j].direction != TerminalNodeDirection.down)))
        {
            return true;
        }

        return false;
    }

    private bool NodeInBounds(int nodeIndex)
    {
        return nodeIndex < nodes.Length && nodeIndex >= 0;
    }

    public void DisplayTerminal(TerminalReaction node)
    {
        MouseController.Instance.OverrideMouse_Terminal = true;
        MissionController.Instance.ShowHide(false);
        //Inventory.Instance.DisplayInventoryGrid(false);
        PlayerMovement.Instance.StopMovement();
        TextManager.Instance.ClearMessage();

        currentTerminal = node.terminal;
        condition = node.condition;
        followUpCollection = node.followUpCollection;
        currentTerminalNumber = node.terminalNumber;
        terminalBackground.gameObject.SetActive(true);
        this.columnCount = node.columnCount;
        this.rowCount = node.matrix.Length / columnCount;

        SetupMatrix(node.matrix);
    }

    private void SetupMatrix(TerminalNode[] matrix)
    {
        foreach (Transform child in lineContainer.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (Transform child in terminalContainer.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        terminalRect.sizeDelta = new Vector2(imageWidth * columnCount, imageWidth * rowCount);
        lineRect.sizeDelta = new Vector2(imageWidth * columnCount, imageWidth * rowCount);

        foreach (var a in matrix)
        {
            var obj = Instantiate(NodePrefab, terminalContainer.transform);
            obj.SetupNode(a);
        }

        nodes = terminalContainer.GetComponentsInChildren<TerminalNodeObject>();

        startImage.anchoredPosition = new Vector2(-terminalRect.rect.width / 2 - startImage.rect.width / 2, terminalRect.rect.height / 2 - startImage.rect.height);
        endImage.anchoredPosition = new Vector2(terminalRect.rect.width / 2 + endImage.rect.width / 2, -terminalRect.rect.height / 2 + endImage.rect.height);

        CalculateRoute();
        //Do it twice because I'm lazy
        CalculateRoute();
    }

    public IEnumerator CloseTerminal()
    {
        yield return new WaitForSeconds(.75f);

        MouseController.Instance.OverrideMouse_Terminal = false;
        terminalBackground.gameObject.SetActive(false);

        if (followUpCollection)
        {
            StartCoroutine(FollowUpReaction());
        }

        PlayerMovement.Instance.AllowMove();

        foreach (Transform child in lineContainer.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        foreach (Transform child in terminalContainer.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
}

