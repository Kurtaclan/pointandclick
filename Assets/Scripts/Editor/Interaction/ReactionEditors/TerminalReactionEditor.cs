using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TerminalReaction))]
public class TerminalReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Terminal Reaction";
    }
}
