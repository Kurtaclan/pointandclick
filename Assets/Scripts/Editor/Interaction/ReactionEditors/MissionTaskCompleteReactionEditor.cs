﻿using UnityEditor;

[CustomEditor(typeof(MissionTaskCompleteReaction))]
public class MissionTaskCompleteReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel ()
    {
        return "Mission Task Complete Reaction";
    }
}
