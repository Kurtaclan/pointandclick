﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightArea : MonoBehaviour
{
    private Light[] Lights;
    public bool onOff;
    private bool prevState;

    private void Awake()
    {
        Lights = GetComponentsInChildren<Light>();
        prevState = onOff;

        setLights();
    }

    private void setLights()
    {
        foreach (var a in Lights)
        {
            a.enabled = onOff;
        }
    }

    private void switchLights()
    {
        onOff = !onOff;
    }

    void LateUpdate()
    {
        if (onOff != prevState)
        {
            prevState = onOff;
            setLights();
        }
    }
}
