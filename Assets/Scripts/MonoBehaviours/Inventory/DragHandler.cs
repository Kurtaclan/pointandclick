﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public static GameObject itemBeingDraged;
    internal Vector3 startPosition;
    internal Transform startParent;

    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        itemBeingDraged = gameObject;
        startPosition = transform.position;
        startParent = transform.parent;
        transform.SetParent(transform.root);
        GetComponent<Image>().raycastTarget = false;
        MouseController.Instance.OverrideMouse_Dragging = true;
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        MouseController.Instance.OverrideMouse_Dragging = false;
        itemBeingDraged = null;
        GetComponent<Image>().raycastTarget = true;

        if (transform.parent == startParent || transform.parent == transform.root)
        {
            transform.position = startPosition;
            transform.SetParent(startParent);
        }
    }
}