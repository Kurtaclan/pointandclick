using UnityEngine;
using System.Linq;
public class AllConditions : ResettableScriptableObject
{
    public Condition[] conditions;
    private static AllConditions instance;
    private const string loadPath = "AllConditions";

    public static AllConditions Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<AllConditions>();
                if (instance)
                {
                    instance.Reset();
                }
            }
            if (!instance)
            {
                instance = Resources.Load<AllConditions>(loadPath);
                if (instance)
                {
                    instance.Reset();
                }
            }
            if (!instance)
            {
                Debug.LogError("AllConditions has not been created yet.  Go to Assets > Create > AllConditions.");
            }

            return instance;
        }
        set { instance = value; }
    }

    public override void Reset()
    {
        if (conditions == null)
            return;

        for (int i = 0; i < conditions.Length; i++)
        {
            conditions[i].satisfied = false;
        }
    }


    public bool CheckConditions(Condition[] requiredConditions)
    {
        return (from a in requiredConditions
                join b in conditions on a.hash equals b.hash
                where a.satisfied != b.satisfied
                select a).Count() == 0;
    }
}
