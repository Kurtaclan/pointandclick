﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
public class AreaTrigger : MonoBehaviour
{
    public bool runOnce;
    private bool ran;
    public ReactionCollection defaultReactionCollection;
    public ConditionCollection[] conditionCollections = new ConditionCollection[0];

    public void OnTriggerEnter(Collider hit)
    {

        if (hit.tag == "Player")
        {
            Trigger();
        }
    }

    public void Trigger()
    {
        foreach (var a in conditionCollections)
        {
            ReactionCollection conditionReaction;

            if (!a.ConditionsPass(out conditionReaction))
            {
                if (conditionReaction)
                {
                    StartCoroutine(interact(conditionReaction));
                }

                return;
            }
        }

        if ((runOnce && !ran) || !runOnce)
        {
            StartCoroutine(interact());
        }
    }

    private IEnumerator interact(ReactionCollection conditionReaction)
    {
        yield return null;
        conditionReaction.End();

        foreach (var a in conditionReaction.reactions)
        {
            if (a is DelayedReaction)
            {
                yield return ((DelayedReaction)a).wait;
            }

            conditionReaction.ReactNext();
        }
    }

    internal virtual IEnumerator interact()
    {
        yield return null;
        defaultReactionCollection.End();

        foreach (var a in defaultReactionCollection.reactions)
        {
            if (a is DelayedReaction)
            {
                yield return ((DelayedReaction)a).wait;
            }

            defaultReactionCollection.ReactNext();
        }

        ran = true;
    }
}
