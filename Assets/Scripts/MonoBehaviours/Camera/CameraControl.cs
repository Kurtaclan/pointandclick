﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraControl : MonoBehaviour
{
    public bool moveCamera = true;
    public bool followCamera = false;
    private bool isCinematic = false;
    private Vector3 previousPosition;
    private Quaternion previousRotation;
    public float cameraSpeed = 3f;
    public Vector2 ScreenBuffer = new Vector2(0.15f, 0.15f);
    public const string startingPositionKey = "starting position";
    public PhysicsRaycaster raycaster;
    private float OriginalYDifference;
    private float OriginalXDifference;
    private float OriginalZDifference;
    private Camera _camera;

    private MapInfo mapInfo;
    private Vector3[] mapBounds;
    private static CameraControl instance;
    public static CameraControl Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<CameraControl>();
            }

            return instance;
        }
    }

    private void Start()
    {
        if ((!moveCamera && !followCamera) || isCinematic)
            return;

        _camera = GetComponent<Camera>();

        if (moveCamera)
        {
            string startingPoint = "";
            PlayerMovement.Instance.playerSaveData.Load(startingPositionKey, ref startingPoint);

            if (!string.IsNullOrEmpty(startingPoint) && startingPoint.Contains("Camera"))
            {
                transform.position = GameObject.Find(startingPoint.Replace("Door", "Camera")).transform.position;
            }

            mapInfo = transform.parent.GetComponent<MapInfo>();
            mapBounds = mapInfo.GetBounds();
        }

        OriginalYDifference = transform.position.y - PlayerMovement.Instance.transform.position.y;
        OriginalXDifference = transform.position.x - PlayerMovement.Instance.transform.position.x;
        OriginalZDifference = transform.position.z - PlayerMovement.Instance.transform.position.z;
    }

    private void LateUpdate()
    {
        if ((!moveCamera && !followCamera) || isCinematic)
            return;

        if (PlayerMovement.Instance.transform.position.y > 8.5f)
        {
            _camera.cullingMask |= (1 << LayerMask.NameToLayer("Second Floor"));
            _camera.cullingMask |= (1 << LayerMask.NameToLayer("Second Floor ViewCulling"));

            raycaster.eventMask |= (1 << LayerMask.NameToLayer("Second Floor"));

            if (MouseController.Instance)
            {
                MouseController.Instance.layerMask |= (1 << LayerMask.NameToLayer("Second Floor"));
            }
        }
        else if (_camera.cullingMask == (_camera.cullingMask | (1 << LayerMask.NameToLayer("Second Floor"))))
        {
            _camera.cullingMask ^= (1 << LayerMask.NameToLayer("Second Floor"));
            _camera.cullingMask ^= (1 << LayerMask.NameToLayer("Second Floor ViewCulling"));

            raycaster.eventMask ^= (1 << LayerMask.NameToLayer("Second Floor"));
            if (MouseController.Instance)
            {
                MouseController.Instance.layerMask ^= (1 << LayerMask.NameToLayer("Second Floor"));
            }
        }

        if (followCamera)
        {
            Vector3 velocity = Vector3.zero;
            var needPos = new Vector3(OriginalXDifference + PlayerMovement.Instance.transform.position.x,
                OriginalYDifference + PlayerMovement.Instance.transform.position.y,
               OriginalZDifference + PlayerMovement.Instance.transform.position.z);
            transform.position = Vector3.SmoothDamp(transform.position, needPos,
                                                    ref velocity, 0.15f);
        }
        else if (moveCamera)
        {
            var screenPoint = Input.mousePosition;
            screenPoint.z = 1.0f;
            Vector3 screenPos = Camera.main.ScreenToViewportPoint(screenPoint);

            if (screenPos.x < ScreenBuffer.x)
            {
                transform.Translate(Vector3.left * cameraSpeed * Time.deltaTime, Space.World);
            }

            if (screenPos.y < ScreenBuffer.y)
            {
                transform.Translate(Vector3.back * cameraSpeed * Time.deltaTime, Space.World);
            }

            if (screenPos.x > (1 - ScreenBuffer.x))
            {
                transform.Translate(Vector3.right * cameraSpeed * Time.deltaTime, Space.World);
            }

            if (screenPos.y > (1 - ScreenBuffer.y))
            {
                transform.Translate(Vector3.forward * cameraSpeed * Time.deltaTime, Space.World);
            }

            if (OriginalYDifference > transform.position.y - PlayerMovement.Instance.transform.position.y)
            {
                transform.Translate(Vector3.up * (cameraSpeed / 2) * Time.deltaTime, Space.World);
            }

            if (OriginalYDifference < transform.position.y - PlayerMovement.Instance.transform.position.y)
            {
                transform.Translate(Vector3.down * (cameraSpeed / 2) * Time.deltaTime, Space.World);
            }

            transform.position = new Vector3(
                Mathf.Clamp(transform.position.x, mapBounds[2].x, mapBounds[0].x),
                transform.position.y,
                Mathf.Clamp(transform.position.z, mapBounds[2].z, mapBounds[0].z));
        }
    }

    internal void CinematicCamMode(bool isCinematic, CanvasGroup faderCanvasGroup, Transform cameraPosition = null)
    {
        this.isCinematic = isCinematic;

        //yield return StartCoroutine(Fade(1f, faderCanvasGroup));

        if (cameraPosition)
        {
            previousPosition = transform.position;
            previousRotation = transform.rotation;

            transform.position = cameraPosition.position;
            transform.rotation = cameraPosition.rotation;
        }
        else if (previousPosition != default(Vector3) && previousRotation != default(Quaternion))
        {
            transform.position = previousPosition;
            transform.rotation = previousRotation;
        }

        // yield return StartCoroutine(Fade(0f, faderCanvasGroup));
    }

    private IEnumerator Fade(float finalAlpha, CanvasGroup faderCanvasGroup)
    {
        faderCanvasGroup.blocksRaycasts = true;

        float fadeSpeed = Mathf.Abs(faderCanvasGroup.alpha - finalAlpha) / 0.2f;

        while (!Mathf.Approximately(faderCanvasGroup.alpha, finalAlpha))
        {
            faderCanvasGroup.alpha = Mathf.MoveTowards(faderCanvasGroup.alpha, finalAlpha,
                fadeSpeed * Time.deltaTime);

            yield return null;
        }

        faderCanvasGroup.blocksRaycasts = false;
    }

    /*
    private void LateUpdate()
    {
        if (!moveCamera || isMoving)
            return;

        Vector3 screenPos = Camera.main.WorldToViewportPoint(player.TargetPosition);

        if (screenPos.x < ScreenBuffer.x || screenPos.y < ScreenBuffer.y ||
            screenPos.x > (1 - ScreenBuffer.x) || screenPos.y > (1 - ScreenBuffer.y))
        {
            StartCoroutine(MoveCamera());
        }
    }

    public IEnumerator MoveCamera()
    {
        yield return new WaitForSeconds(1f);

        Vector3 screenPos = Camera.main.WorldToViewportPoint(player.TargetPosition);

        if (screenPos.x < ScreenBuffer.x)
        {
            transform.Translate(Vector3.left * cameraSpeed * Time.deltaTime, Space.World);
        }

        if (screenPos.y < ScreenBuffer.y)
        {
            transform.Translate(Vector3.back * cameraSpeed * Time.deltaTime, Space.World);
        }

        if (screenPos.x > (1 - ScreenBuffer.x))
        {
            transform.Translate(Vector3.right * cameraSpeed * Time.deltaTime, Space.World);
        }

        if (screenPos.y > (1 - ScreenBuffer.y))
        {
            transform.Translate(Vector3.forward * cameraSpeed * Time.deltaTime, Space.World);
        }
    }
    */
}
