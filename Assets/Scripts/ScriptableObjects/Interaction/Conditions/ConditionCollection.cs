﻿using UnityEngine;

public class ConditionCollection : ScriptableObject
{
    public string description;
    public Condition[] requiredConditions = new Condition[0];
    public ReactionCollection reactionCollection;

    public bool ConditionsPass(out ReactionCollection reactionCollection)
    {
        reactionCollection = this.reactionCollection;
        return AllConditions.Instance.CheckConditions(requiredConditions);
    }
}