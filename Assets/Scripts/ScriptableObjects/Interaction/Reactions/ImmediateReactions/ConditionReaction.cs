﻿using System;
using System.Linq;

public class ConditionReaction : Reaction
{
    public Condition condition;
    public bool satisfied;

    protected override void ImmediateReaction()
    {
        AllConditions.Instance.conditions.FirstOrDefault(x => x.hash == condition.hash).satisfied = satisfied;
    }

    public override void End()
    {
        //Nothing to see here
    }
}
