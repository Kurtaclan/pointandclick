﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class MovingInteractable : Interactable
{
    public PathNodeContainer nodeContainer;
    public NavMeshAgent agent;
    public float turnSpeedThreshold = 0.5f;
    public float turningSpeed = 15f;
    public bool Paused;

    private void Start()
    {
        NavigateToNextNode();
    }

    private void Update()
    {
        interactionLocation.LookAt(transform);

        if (agent.pathPending) { return; }
        MoveLogic();
    }

    internal virtual void MoveLogic()
    {
        if (!Paused)
        {
            if (agent.isStopped)
            {
                agent.isStopped = false;
            }

            float speed = agent.desiredVelocity.magnitude;

            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                NavigateToNextNode();
            }

            if (speed > turnSpeedThreshold)
            {
                Moving();
            }
        }
        else
        {
            agent.isStopped = true;
        }
    }

    internal override void BeginInteract()
    {
        Paused = true;
    }

    internal void NavigateToNextNode()
    {
        agent.SetDestination(nodeContainer.NextNode());
    }

    internal void Moving()
    {
        Quaternion targetRotation = Quaternion.LookRotation(agent.desiredVelocity);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, turningSpeed * Time.deltaTime);
    }

    internal override bool EndInteraction()
    {
        Paused = false;
        return base.EndInteraction();
    }
}
