﻿using System;
using System.Collections;
using UnityEngine;

public class ElevatorInteractable : Interactable
{
    public ElevatorState state;
    public Transform ElevatorPlatform;
    public float BottomHeight;
    public float TopHeight;
    private float playerHeightDif;
    private Vector3 velocity = Vector3.zero;
    public IEnumerator Elevate()
    {
        velocity = Vector3.zero;
        PlayerMovement.Instance.agent.enabled = false;
        playerHeightDif = PlayerMovement.Instance.transform.position.y - ElevatorPlatform.position.y;
        yield return new WaitForSeconds(0.9f);
        state = state == ElevatorState.Top ? ElevatorState.ElevatingDown : ElevatorState.ElevatingUp;
    }

    internal override bool EndInteraction()
    {
        return state == ElevatorState.Bottom || state == ElevatorState.Top;
    }

    public override bool Interact()
    {
        StartCoroutine(Elevate());

        return base.Interact();
    }

    private void LateUpdate()
    {
        var needPos = new Vector3(ElevatorPlatform.position.x, state == ElevatorState.ElevatingUp ? TopHeight : BottomHeight, ElevatorPlatform.position.z);

        if (state == ElevatorState.ElevatingUp || state == ElevatorState.ElevatingDown)
        {
            ElevatorPlatform.position = Vector3.SmoothDamp(ElevatorPlatform.position, needPos,
                                            ref velocity, 1.5f);

            PlayerMovement.Instance.transform.position = new Vector3(PlayerMovement.Instance.transform.position.x,
                ElevatorPlatform.position.y + playerHeightDif, PlayerMovement.Instance.transform.position.z);

            if (Vector3.Distance(ElevatorPlatform.position, needPos) < 0.03f)
            {
                ElevatorPlatform.position = needPos;
                PlayerMovement.Instance.transform.position = new Vector3(PlayerMovement.Instance.transform.position.x,
              ElevatorPlatform.position.y + playerHeightDif, PlayerMovement.Instance.transform.position.z);
                state = state == ElevatorState.ElevatingUp ? ElevatorState.Top : ElevatorState.Bottom;
                PlayerMovement.Instance.agent.enabled = true;
            }
        }
    }
}

public enum ElevatorState
{
    Bottom,
    Top,
    ElevatingUp,
    ElevatingDown
}
