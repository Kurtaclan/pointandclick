using UnityEngine;
using UnityEngine.AI;

public class RandomFloorBotMovement : MonoBehaviour
{
    public float walkRadius;

    private NavMeshAgent agent;
    public Vector3 currentDestination;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        SetRandomDestination();
    }

    void FixedUpdate()
    {
        if (Vector3.Distance(transform.position, agent.destination) <= agent.stoppingDistance || (agent.velocity.x == 0 && agent.velocity.z == 0))
            SetRandomDestination();
    }


    void SetRandomDestination()
    {
        Vector3 randomDirection = Random.insideUnitSphere * walkRadius;
        randomDirection += transform.position;
        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, walkRadius, 1);
        agent.destination = currentDestination = hit.position;
    }
}