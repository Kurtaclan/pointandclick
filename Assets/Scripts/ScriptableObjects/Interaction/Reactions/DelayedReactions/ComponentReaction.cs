﻿using System;
using UnityEngine;

public class ComponentReaction : DelayedReaction
{
    public GameObject gameObject;
    public string componentType;

    protected override void ImmediateReaction()
    {
        var component = gameObject.GetComponent(componentType) as MonoBehaviour;
        component.enabled = !component.enabled;
    }

    public override void End()
    {
        //Nothing to see here
    }
}