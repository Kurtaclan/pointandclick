﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System;

public class Interactable : MonoBehaviour
{
    public Transform interactionLocation;
    public ConditionCollection[] conditionCollections = new ConditionCollection[0];
    public ReactionCollection defaultReactionCollection;
    public bool Interacting = false;

    private void Start()
    {
        if (interactionLocation)
        {
            interactionLocation.eulerAngles = new Vector3(0, interactionLocation.eulerAngles.y, 0);
        }
    }

    public virtual bool Interact()
    {
        foreach (var a in conditionCollections)
        {
            ReactionCollection conditionReaction;

            if (!a.ConditionsPass(out conditionReaction))
            {
                if (conditionReaction)
                {
                    StartCoroutine(interact(conditionReaction));
                }
                else
                {
                    EndInteraction();
                }

                return false;
            }
        }

        Interacting = true;
        StartCoroutine(interact());
        return true;
    }

    private IEnumerator interact(ReactionCollection conditionReaction)
    {
        yield return null;
        conditionReaction.End();

        foreach (var a in conditionReaction.reactions)
        {
            if (a is DelayedReaction)
            {
                yield return ((DelayedReaction)a).wait;
            }

            conditionReaction.ReactNext();
        }

        Interacting = false;
    }

    internal virtual void BeginInteract()
    {
        //does nothing in base interactable, but can be implemented if needed
    }

    internal virtual IEnumerator interact()
    {
        yield return null;
        defaultReactionCollection.End();

        foreach (var a in defaultReactionCollection.reactions)
        {
            if (a is DelayedReaction)
            {
                yield return ((DelayedReaction)a).wait;
            }

            defaultReactionCollection.ReactNext();
        }

        Interacting = false;
    }

    internal void ItemDrop(Item item)
    {
        if (defaultReactionCollection.reactions.Count(x => x is LostItemReaction) > 0)
        {
            var reaction = defaultReactionCollection.reactions.FirstOrDefault(x => x is LostItemReaction) as LostItemReaction;

            if (reaction.itemName == item.ItemName)
            {
                PlayerMovement.Instance.Interact(this);
            }
        }
    }

    internal virtual bool EndInteraction()
    {
        StopAllCoroutines();

        if (defaultReactionCollection)
        {
            defaultReactionCollection.End();
        }
               
        Interacting = false;

        return true;
    }
}
