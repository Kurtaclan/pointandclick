﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Transform))]
class WorldPosViewer : Editor
{
    Transform obj;
    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        obj = (Transform)target;
        GUI.enabled = false;
        EditorGUILayout.Vector3Field("World Position", new Vector3(obj.position.x, obj.position.y, obj.position.z));        
        obj.eulerAngles = EditorGUILayout.Vector3Field("World Rotation", obj.eulerAngles);
        GUI.enabled = true;
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        obj.localPosition = EditorGUILayout.Vector3Field("Local Position", new Vector3(obj.localPosition.x, obj.localPosition.y, obj.localPosition.z));
        obj.localEulerAngles = EditorGUILayout.Vector3Field("Local Rotation", obj.localEulerAngles);
        obj.localScale = EditorGUILayout.Vector3Field("Local Scale", obj.localScale);

        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, "Transform Modified");
        }
    }
}