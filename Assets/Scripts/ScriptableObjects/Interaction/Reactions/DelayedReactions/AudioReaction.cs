﻿using System;
using UnityEngine;

public class AudioReaction : DelayedReaction
{
    public AudioClip audioClip;

    protected override void ImmediateReaction()
    {
        AudioController.Instance.effects.clip = audioClip;
        AudioController.Instance.effects.Play();
    }

    public override void End()
    {
        AudioController.Instance.effects.Stop();
    }
}