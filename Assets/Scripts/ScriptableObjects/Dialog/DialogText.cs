﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class DialogText : MonoBehaviour
{
    public Text text;
    internal DialogNode node;
    private TextMesh dialogMesh;
    Vector2 ogSize;

    void Awake()
    {
        ogSize = text.rectTransform.sizeDelta;
    }

    public void OnClick()
    {
        if (node != null)
        {
            DialogManager.Instance.DisplayMessage(node, dialogMesh);
        }
    }

    public void SetNode(DialogNode dialogNode, TextMesh textMesh)
    {
        node = dialogNode;
        dialogMesh = textMesh;

        if (node != null && node.dialogText != text.text)
        {
            text.text = node.dialogText;
            text.rectTransform.sizeDelta = ogSize;
        }
        else if (node == null)
        {
            text.text = "";
            text.rectTransform.sizeDelta = new Vector2(0, 0);
        }
    }

    internal void ClearNode()
    {
        text.text = "";
        node = null;
        text.rectTransform.sizeDelta = new Vector2(0, 0);
    }
}
