﻿using UnityEngine;
public class AutoDoor : MonoBehaviour
{    
    public GameObject character;
    public float distanceToOpen = 5;
    private Animator anim;
    private int characterNearbyHash = Animator.StringToHash("character_nearby");

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        var distance = Vector3.Distance(transform.position, character.transform.position);

        if (distanceToOpen >= distance)
        {
            anim.SetBool(characterNearbyHash, true);
        }
        else
        {
            anim.SetBool(characterNearbyHash, false);
        }
    }
}
