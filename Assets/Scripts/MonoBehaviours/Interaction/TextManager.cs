using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class TextManager : MonoBehaviour
{
    public struct Message
    {
        public string message;
        public Color textColor;
        public float startTime;
    }

    public Text text;
    public GameObject textContainer;

    public float displayTimePerCharacter = 0.1f;
    public float additionalDisplayTime = 0.5f;

    private List<Message> instructions = new List<Message>();
    private float clearTime;

    private static TextManager instance;

    public static TextManager Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<TextManager>();
            }

            return instance;
        }
    }

    private void Start()
    {
        textContainer.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (instructions.Count > 0 && Time.time >= instructions[0].startTime)
        {
            textContainer.gameObject.SetActive(true);
            text.text = instructions[0].message;
            text.color = instructions[0].textColor;
            instructions.RemoveAt(0);
        }
        else if (Time.time >= clearTime)
        {
            text.text = string.Empty;
            textContainer.gameObject.SetActive(false);
        }
    }

    public void DisplayMessage(string message, Color textColor, float delay)
    {
        float startTime = Time.time + delay;
        float displayDuration = message.Length * displayTimePerCharacter + additionalDisplayTime;
        float newClearTime = startTime + displayDuration;

        if (newClearTime > clearTime)
            clearTime = newClearTime;

        Message newInstruction = new Message
        {
            message = message,
            textColor = textColor,
            startTime = startTime,
        };

        instructions.Add(newInstruction);
    }

    public void ClearMessage()
    {
        instructions = new List<Message>();
        text.text = string.Empty;
        textContainer.gameObject.SetActive(false);
    }
}

