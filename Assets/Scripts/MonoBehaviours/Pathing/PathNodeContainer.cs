﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PathNodeContainer : MonoBehaviour
{
    public PathNode[] Nodes;
    private int CurNode = -1;
    private void Awake()
    {
        Nodes = GetComponentsInChildren<PathNode>();
    }

    public Vector3 NextNode()
    {
        CurNode++;

        if (CurNode == Nodes.Length)
        {
            CurNode = 0;
        }

        return Nodes[CurNode].transform.position;
    }

    public Vector3 CurrentNode()
    {
        return Nodes[CurNode].transform.position;
    }
}