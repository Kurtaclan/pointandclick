﻿using UnityEditor;

[CustomEditor(typeof(PlayerStopReaction))]
public class PlayerStopReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel ()
    {
        return "Player Stop Reaction";
    }
}
