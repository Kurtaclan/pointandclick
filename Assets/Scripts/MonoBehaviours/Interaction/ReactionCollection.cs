﻿using System;
using UnityEngine;

public class ReactionCollection : MonoBehaviour
{
    public Reaction[] reactions;
    private int index = 0;

    private void Start()
    {
        for (int i = 0; i < reactions.Length; i++)
        {
            if (reactions[i] is DelayedReaction)
                ((DelayedReaction)reactions[i]).Init();
            else
            {
                reactions[i].Init();
            }
        }
    }

    public void ReactNext()
    {
        if (index >= reactions.Length)
            return;

        reactions[index].React(this);
        index++;
    }

    internal void End()
    {
        foreach (var reaction in reactions)
        {
            reaction.End();
        }

        index = 0;
    }
}
