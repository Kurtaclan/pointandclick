using UnityEditor;

[CustomEditor(typeof(ComponentReaction))]
public class ComponentReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel ()
    {
        return "Component Reaction";
    }
}
