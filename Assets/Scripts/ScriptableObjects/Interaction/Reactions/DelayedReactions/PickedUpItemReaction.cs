﻿using System;

public class PickedUpItemReaction : DelayedReaction
{
    public Item item;
    protected override void SpecificInit()
    {
       
    }

    protected override void ImmediateReaction()
    {
        Inventory.Instance.AddItem(item);        
    }

    public override void End()
    {
        //Nothing to see here
    }
}
