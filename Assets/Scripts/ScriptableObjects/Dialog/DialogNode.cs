﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class DialogNode : MonoBehaviour
{
    public DialogNode[] nodes;

    public string dialogText;
    public string responseText;
    public Color dialogColor = Color.white;
}
