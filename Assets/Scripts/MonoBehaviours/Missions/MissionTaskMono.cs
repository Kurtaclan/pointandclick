﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class MissionTaskMono : MonoBehaviour
{
    MissionTask task;
    public Text missionTaskTitle;
    public Image missionTaskCompleteImage;
    bool complete = false;

    private void Start()
    {
        missionTaskCompleteImage.gameObject.SetActive(complete);
    }

    public void Created(MissionTask task)
    {
        this.task = task;
        missionTaskTitle.text = this.task.TaskTitle;
        complete = this.task.TaskComplete;
        missionTaskCompleteImage.gameObject.SetActive(complete);
    }

    public void Update()
    {
        if (task == null)
        {
            return;
        }

        if (task.TaskComplete != complete)
        {
            complete = task.TaskComplete;
            StartCoroutine(MarkTaskComplete());
        }
    }

    private IEnumerator MarkTaskComplete()
    {
        yield return new WaitForSeconds(0.5f);
        missionTaskCompleteImage.gameObject.SetActive(true);
    }
}
