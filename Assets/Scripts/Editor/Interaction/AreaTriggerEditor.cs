using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AreaTrigger))]
public class AreaTriggerEditor : EditorWithSubEditors<ConditionCollectionEditor, ConditionCollection>
{
    private AreaTrigger trigger;
    private SerializedProperty runOnceProperty;
    private SerializedProperty collectionsProperty;
    private SerializedProperty defaultReactionCollectionProperty;

    private const float collectionButtonWidth = 150f;
    private const string runOnceName = "runOnce";
    private const string conditionCollectionsName = "conditionCollections";
    private const string defaultReactionCollectionName = "defaultReactionCollection";

    private void OnEnable()
    {
        trigger = (AreaTrigger)target;

        runOnceProperty = serializedObject.FindProperty(runOnceName);
        collectionsProperty = serializedObject.FindProperty(conditionCollectionsName);
        defaultReactionCollectionProperty = serializedObject.FindProperty(defaultReactionCollectionName);

        CheckAndCreateSubEditors(ref trigger.conditionCollections);
    }

    private void OnDisable()
    {
        CleanupEditors();
    }

    protected override void SubEditorSetup(ConditionCollectionEditor editor)
    {
        editor.collectionsProperty = collectionsProperty;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        CheckAndCreateSubEditors(ref trigger.conditionCollections);

        EditorGUILayout.PropertyField(runOnceProperty);

        for (int i = 0; i < subEditors.Length; i++)
        {
            subEditors[i].OnInspectorGUI();
            EditorGUILayout.Space();
        }

        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Add Condition Collection", GUILayout.Width(collectionButtonWidth)))
        {
            ConditionCollection newCollection = ConditionCollectionEditor.CreateConditionCollection();
            collectionsProperty.AddToObjectArray(newCollection);
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(defaultReactionCollectionProperty);

        if (GUILayout.Button("Test Trigger"))
        {
            trigger.Trigger();
        }

        serializedObject.ApplyModifiedProperties();
    }
}
