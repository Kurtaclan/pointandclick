﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CameraMask : MonoBehaviour
{
    public LayerMask layerMask = 0;
    public float fadeSpeed = 1.0f;
    public float fadedOutAlpha = 0.3f;
    public Camera _camera;

    private List<FadeoutLOSInfo> fadedOutObjects = new List<FadeoutLOSInfo>();

    class FadeoutLOSInfo
    {
        public Renderer renderer;
        public Material[] originalMaterials;
        public Material[] alphaMaterials;
        public bool needFadeOut = true;
    }

    FadeoutLOSInfo FindLosInfo(Renderer r)
    {
        var fade = fadedOutObjects.FirstOrDefault(x => x.renderer == r);
        return fade;
    }

    private void LateUpdate()
    {
        // Now go over all renderers and do the actual fading!
        var fadeDelta = fadeSpeed * Time.deltaTime;
        for (int i = 0; i < fadedOutObjects.Count; i++)
        {
            var fade = fadedOutObjects[i];
            // Fade out up to minimum alpha value

            if (fade.needFadeOut)
            {
                foreach (var alphaMaterial in fade.alphaMaterials)
                {
                    var alpha = alphaMaterial.color.a;
                    alpha -= fadeDelta;
                    alpha = Mathf.Max(alpha, fadedOutAlpha);
                    Color color = alphaMaterial.color;
                    color.a = alpha;
                    alphaMaterial.color = color;
                }
            }
            // Fade back in
            else
            {
                var totallyFadedIn = 0;
                foreach (var alphaMaterial in fade.alphaMaterials)
                {
                    var alpha = alphaMaterial.color.a;
                    alpha += fadeDelta;
                    alpha = Mathf.Min(alpha, 1.0f);
                    Color color = alphaMaterial.color;
                    color.a = alpha;
                    alphaMaterial.color = color;

                    if (alpha >= 0.99)
                        totallyFadedIn++;
                }

                // All alpha materials are faded back to 100%
                // Thus we can switch back to the original materials
                if (totallyFadedIn == fade.alphaMaterials.Length)
                {
                    if (fade.renderer)
                        fade.renderer.sharedMaterials = fade.originalMaterials;

                    foreach (var newMaterial in fade.alphaMaterials)
                        Destroy(newMaterial);

                    fadedOutObjects.RemoveAt(i);
                    i--;
                }
            }
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (layerMask != (layerMask | (1 << collision.gameObject.layer)))
        {
            return;
        }

        var obj = fadedOutObjects.FirstOrDefault(x => x.renderer == collision.collider.GetComponent<Renderer>());

        if (obj != null)
        {
            obj.needFadeOut = false;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if(layerMask != (layerMask | (1 << collision.gameObject.layer)))
        {
            return;
        }

        // Make sure we have a renderer
        Renderer hitRenderer = collision.collider.GetComponent<Renderer>();

        if (hitRenderer == null || !hitRenderer.enabled)
        {
            return;
        }

        var info = FindLosInfo(hitRenderer);

        // We are not fading this renderer already, so insert into faded objects map
        if (info == null)
        {
            info = new FadeoutLOSInfo();
            info.originalMaterials = hitRenderer.sharedMaterials;
            info.alphaMaterials = new Material[info.originalMaterials.Length];
            info.renderer = hitRenderer;

            for (var i = 0; i < info.originalMaterials.Length; i++)
            {
                var newMaterial = new Material(Shader.Find("Alpha/Diffuse"));
                newMaterial.mainTexture = info.originalMaterials[i].mainTexture;
                newMaterial.color = info.originalMaterials[i].color;
                Color color = newMaterial.color;
                color.a = 1.0f;
                newMaterial.color = color;
                info.alphaMaterials[i] = newMaterial;
            }

            hitRenderer.sharedMaterials = info.alphaMaterials;
            fadedOutObjects.Add(info);
        }
        // Just mark the renderer as needing fade out
        else
        {
            info.needFadeOut = true;
        }
    }
}
