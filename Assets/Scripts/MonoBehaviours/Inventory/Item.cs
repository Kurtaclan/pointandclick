﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Item : DragHandler
{
    public Image Image;
    public bool IsUsable;
    public string Description;
    public string ItemName;

    public virtual void UseItem()
    {
        //Do Nothing By Default
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        var slot = eventData.pointerEnter.transform.parent.GetComponent<GridSlot>();
        var interactable = eventData.pointerEnter.transform.GetComponent<Interactable>();
        if (slot && !slot.GetComponentInChildren<Item>())
        {
            if (!(slot is ActiveItemSlot))
            {
                startParent.GetComponent<GridSlot>().SetItem(null);
                transform.SetParent(eventData.pointerEnter.transform.parent);
                transform.localPosition = new Vector3(0, 0, 0);
            }

            slot.SetItem(this);
        }
        else if (interactable)
        {
            interactable.ItemDrop(this);
        }

        base.OnEndDrag(eventData);
    }
}
