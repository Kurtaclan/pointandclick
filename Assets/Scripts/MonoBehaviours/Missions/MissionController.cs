﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class MissionController : MonoBehaviour
{
    public GameObject uiPanel;
    public Transform missionList;
    public MissionMono missionPrefab;
    public List<Mission> Missions = new List<Mission>();
    private static MissionController instance;

    public static MissionController Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<MissionController>();
            }

            return instance;
        }
    }

    void Start()
    {
        uiPanel.SetActive(false);
    }

    public void ShowHide()
    {
        ShowHide(!uiPanel.activeSelf);
    }

    public void ShowHide(bool showHide)
    {
        uiPanel.SetActive(showHide);

        if (!uiPanel.activeSelf)
        {
            MouseController.Instance.OverrideMouse_MissionUI = false;
        }
    }

    public void LateUpdate()
    {
        if (Input.GetKeyUp(KeyCode.L) && !MouseController.Instance.OverrideMouse_Dialog &&!MouseController.Instance.OverrideMouse_Terminal)
        {
            ShowHide();
        }
    }

    internal bool CheckMission(string missionTitle)
    {
        return Missions.Exists(x => x.MissionTitle == missionTitle);
    }

    internal bool CheckMissionTask(string taskTitle)
    {
        return Missions.Exists(x => x.MissionTasks.Count(y => y.TaskTitle == taskTitle) > 0);
    }

    internal void MissionTaskComplete(string taskTitle)
    {
        var mission = Missions.FirstOrDefault(x => x.MissionTasks.Count(y => y.TaskTitle == taskTitle) > 0);

        if (mission == null)
        {
            return;
        }

        var task = mission.MissionTasks.FirstOrDefault(x => x.TaskTitle == taskTitle);
        task.TaskComplete = true;

        if (mission.MissionTasks.Count(x => !x.TaskComplete) == 0)
        {
            mission.MissionComplete = true;
        }
    }

    internal void AddMission(Mission missionToAdd)
    {
        Missions.Add(missionToAdd);
        var mission = Instantiate(missionPrefab, missionList);
        mission.Created(missionToAdd);
    }
}
