﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssemblyLine : MonoBehaviour
{
    public Animator animator;
    public Animator[] children;
    public AutoDoor door;

    void Awake()
    {
        animator.enabled = false;
        children = GetComponentsInChildren<Animator>();
        StartCoroutine(EnableRollers(animator.enabled));
    }

    void EnableAssembly()
    {
        animator.enabled = !animator.enabled;
        door.enabled = animator.enabled;
        StartCoroutine(EnableRollers(animator.enabled));
    }

    private IEnumerator EnableRollers(bool state)
    {
        yield return null;

        foreach(var a in children)
        {
            a.enabled = state;
        }
    }
}
