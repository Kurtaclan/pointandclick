﻿using UnityEngine;
using System.Collections;
using System;

public class SeeingInteractable : MovingInteractable
{
    public bool stationary = false;
    public bool moveToPlayer = true;
    public bool canSeePlayer = true;
    public float timeToReset = 6f;
    internal override void MoveLogic()
    {
        if (!Paused)
        {
            if (canSeePlayer && CanSeePlayer())
            {
                if (moveToPlayer)
                {
                    agent.stoppingDistance = 1.5f;
                    agent.SetDestination(PlayerMovement.Instance.transform.position);
                }

                PlayerMovement.Instance.Interact(this, moveToPlayer, !stationary);
            }
            if (!stationary)
            {
                if (agent.isStopped)
                {
                    agent.isStopped = false;
                }

                float speed = agent.desiredVelocity.magnitude;

                if (agent.remainingDistance <= agent.stoppingDistance)
                {
                    NavigateToNextNode();
                }

                if (speed > turnSpeedThreshold)
                {
                    Moving();
                }
            }
            else if (stationary)
            {
                if (!agent.isStopped)
                {
                    agent.isStopped = true;
                }

                if (Turning() > 0.85)
                {
                    NavigateToNextNode();
                }
            }

            if (!canSeePlayer)
            {
                StartCoroutine(EnableVision());
            }
        }
    }

    private IEnumerator EnableVision()
    {
        yield return new WaitForSeconds(timeToReset);
        canSeePlayer = true;
    }

    private float Turning()
    {
        Vector3 targetDir = agent.destination - transform.position;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, new Vector3(targetDir.x, 0, targetDir.z), turningSpeed * Time.deltaTime, 0.0F);
        Debug.DrawRay(transform.position, newDir, Color.red);
        transform.rotation = Quaternion.LookRotation(newDir);
        return Vector3.Dot(targetDir.normalized, transform.forward);
    }

    bool CanSeePlayer()
    {
        RaycastHit hit;
        var rayDirection = PlayerMovement.Instance.transform.position - transform.position;
        var distanceToPlayer = Vector3.Distance(transform.position, PlayerMovement.Instance.transform.position);

        if (Physics.Raycast(transform.position, rayDirection, out hit, 100f, LayerMask.GetMask("Characters")))
        {
            if ((hit.transform.tag == "Player") && (distanceToPlayer <= 2.2f))
            {
                return true;
            }
        }

        if ((Vector3.Angle(rayDirection, transform.forward)) < 25)
        {
             if (Physics.Raycast(transform.position, rayDirection, out hit, 100f, LayerMask.GetMask("Characters")))
            {
                if (hit.transform.tag == "Player")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        return false;
    }

    public override bool Interact()
    {
        canSeePlayer = false;
        return base.Interact();
    }

    internal override bool EndInteraction()
    {
        base.EndInteraction();

        canSeePlayer = false;
        PlayerMovement.Instance.AllowMove();
        agent.stoppingDistance = 0;
        NavigateToNextNode();

        return true;
    }

    internal override IEnumerator interact()
    {
        defaultReactionCollection.End();
        foreach (var a in defaultReactionCollection.reactions)
        {
            if (a is DelayedReaction)
            {
                yield return ((DelayedReaction)a).wait;
            }

            defaultReactionCollection.ReactNext();
        }

        Interacting = false;
        yield return new WaitForSeconds(0.5f);
        PlayerMovement.Instance.AllowMove();
        agent.stoppingDistance = 0;
        NavigateToNextNode(); 
    }
}
