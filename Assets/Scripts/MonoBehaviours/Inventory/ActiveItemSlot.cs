﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ActiveItemSlot : GridSlot
{
    public Image slotImage;
    public void UseItem()
    {
        if (slotItem)
        {
            slotItem.UseItem();
        }
    }

    private void Awake()
    {
        if (slotItem)
        {
            slotImage.sprite = slotItem.Image.sprite;
            slotImage.enabled = true;
        }
    }

    public override void SetItem(Item item, bool destroy = false)
    {
        if (!item.IsUsable || !Inventory.Instance.CheckDuplicateActiveItems(item))
        {
            return;
        }

        slotItem = item;
        if (slotItem)
        {
            slotImage.sprite = slotItem.Image.sprite;
            slotImage.enabled = true;
        }
        else
        {
            slotImage.sprite = null;
            slotImage.enabled = false;
        }
    }
}
