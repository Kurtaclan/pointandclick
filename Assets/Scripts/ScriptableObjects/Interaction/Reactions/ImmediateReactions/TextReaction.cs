﻿using System;
using UnityEngine;

public class TextReaction : Reaction
{
    public string message;
    public Color textColor = Color.white;
    public float delay;

    protected override void SpecificInit()
    {
        
    }

    protected override void ImmediateReaction()
    {
        TextManager.Instance.DisplayMessage(message, textColor, delay);
    }

    public override void End()
    {
        TextManager.Instance.ClearMessage();
    }
}