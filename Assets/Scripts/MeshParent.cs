﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MeshParent : MonoBehaviour
{
    private void Awake()
    {
        var meshCollider = GetComponent<MeshFilter>();
        meshCollider.sharedMesh = null;
        var meshColliders = GetComponentsInChildren<MeshFilter>();
        var combine = new List<CombineInstance>();

        foreach (var a in meshColliders.Where(x => x.sharedMesh))
        {
            combine.Add(new CombineInstance() { mesh = a.sharedMesh, transform = a.transform.localToWorldMatrix });
            a.gameObject.SetActive(false);
        }

        var mesh = new Mesh();
        mesh.CombineMeshes(combine.ToArray());
        meshCollider.sharedMesh = mesh;
    }    
}
