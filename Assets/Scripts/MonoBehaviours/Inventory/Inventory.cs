﻿using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System;

public class Inventory : MonoBehaviour
{
    public ActiveItemSlot[] activeItems = new ActiveItemSlot[numItemSlots];
    public InventoryGrid InventoryGrid;
    public const int numItemSlots = 3;
    private static Inventory instance;
    public static Inventory Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<Inventory>();
            }

            return instance;
        }
    }

    public void DisplayInventoryGrid(bool showHide)
    {
        InventoryGrid.ShowHide(showHide);
    }

    public void DisplayInventoryGrid()
    {
        InventoryGrid.ShowHide();
    }

    internal bool CheckDuplicateActiveItems(Item item)
    {
        return !(activeItems.Count(x => x.slotItem == item) > 0);
    }

    public void LateUpdate()
    {
        if ((Input.GetKeyUp(KeyCode.I) || Input.GetKeyUp(KeyCode.B)) && !MouseController.Instance.OverrideMouse_Dialog && !MouseController.Instance.OverrideMouse_Terminal)
        {
            DisplayInventoryGrid();
        }
    }

    public void AddItem(Item itemToAdd)
    {
        InventoryGrid.AddItem(itemToAdd);
    }

    public void RemoveItem(string itemToRemove)
    {
        InventoryGrid.RemoveItem(itemToRemove);

        // Not utilized in current gamestate
        //var slot = activeItems.FirstOrDefault(x => x.slotItem.ItemName == itemToRemove);

        //if (slot)
        //{
        //    slot.SetItem(null);
        //}
    }
}
