﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;

public class InventoryGrid : MonoBehaviour
{
    public GameObject uiPanel;
    public GridSlot[] Grid;

    void Start()
    {
        uiPanel.SetActive(false);
        Grid = GetComponentsInChildren<GridSlot>();
    }

    internal void ShowHide()
    {
        ShowHide(!uiPanel.activeSelf);
    }

    internal void RemoveItem(string itemToRemove)
    {
        var slot = Grid.FirstOrDefault(x => x.slotItem != null && x.slotItem.ItemName == itemToRemove);

        if (slot)
        {
            slot.SetItem(null, true);
        }
    }

    internal void ShowHide(bool showHide)
    {
        uiPanel.SetActive(showHide);

        if (!uiPanel.activeSelf)
        {
            MouseController.Instance.OverrideMouse_InventoryGrid = false;
        }
    }

    internal void AddItem(Item itemToAdd)
    {
        Grid.FirstOrDefault(x => x.slotItem == null).AddItem(itemToAdd);
    }
}
