﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MissionStartReaction))]
public class MissionStartReactionEditor : ReactionEditor
{
    private const float buttonWidth = 30f;

    protected override string GetFoldoutLabel()
    {
        return "Mission Start Reaction";
    }

    protected override void DrawReaction()
    {
        var targetObject = serializedObject.targetObject as MissionStartReaction;

        if (targetObject.mission == null)
        {
            targetObject.mission = new Mission();
        }

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Delay");
        targetObject.delay = EditorGUILayout.FloatField(targetObject.delay);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Mission Title");
        targetObject.mission.MissionTitle = EditorGUILayout.TextField(targetObject.mission.MissionTitle);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Separator();
        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUILayout.LabelField("Mission Tasks");
        EditorGUI.indentLevel++;

        if (targetObject.mission.MissionTasks == null)
        {
            targetObject.mission.MissionTasks = new MissionTask[0];
        }

        for (int i = 0; i < targetObject.mission.MissionTasks.Length; i++)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Mission Task Title");
            targetObject.mission.MissionTasks[i].TaskTitle = EditorGUILayout.TextField(targetObject.mission.MissionTasks[i].TaskTitle);
            EditorGUILayout.EndHorizontal();
        }

        EditorGUI.indentLevel--;

        EditorGUILayout.Separator();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Add/Remove Tasks");

        //add Mission Task
        if (GUILayout.Button("+", GUILayout.Width(buttonWidth)))
        {
            MissionTask[] array = new MissionTask[targetObject.mission.MissionTasks.Length + 1];

            targetObject.mission.MissionTasks.CopyTo(array, 0);
            targetObject.mission.MissionTasks = array;
            targetObject.mission.MissionTasks[targetObject.mission.MissionTasks.Length - 1] = new MissionTask();
        }

        //Remove Mission Task
        if (targetObject.mission.MissionTasks.Length > 0)
        {
            if (GUILayout.Button("-", GUILayout.Width(buttonWidth)))
            {
                var array = targetObject.mission.MissionTasks.ToList();
                array.RemoveAt(array.Count - 1);
                targetObject.mission.MissionTasks = array.ToArray();
            }
        }

        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Separator();
        EditorGUILayout.EndVertical();

        if (GUI.changed)
        {
            Undo.RecordObject(target, "Changed Mission");
        }
    }
}
