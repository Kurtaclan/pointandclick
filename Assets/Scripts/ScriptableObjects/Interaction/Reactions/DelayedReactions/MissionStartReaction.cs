﻿using System;
using UnityEngine;

public class MissionStartReaction : DelayedReaction
{
    public Mission mission;

    protected override void ImmediateReaction()
    {
        MissionController.Instance.ShowHide(true);
        MissionController.Instance.AddMission(mission);
    }

    public override void End()
    {
    }
}