﻿using System;
using UnityEngine;

public class BroadcastReaction : DelayedReaction
{
    public GameObject gameObject;
    public string methodName;
    public string parameter;

    protected override void ImmediateReaction()
    {
        gameObject.BroadcastMessage(methodName, parameter);
    }

    public override void End()
    {
        //Nothing to see here
    }
}