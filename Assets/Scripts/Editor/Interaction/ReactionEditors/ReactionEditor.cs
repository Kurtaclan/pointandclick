using System;
using UnityEngine;
using UnityEditor;

public abstract class ReactionEditor : Editor
{
    public bool showReaction;
    public SerializedProperty reactionsProperty;
    public ReactionCollectionEditor editorParent;

    private Reaction reaction;

    private const float buttonWidth = 30f;

    private void OnEnable()
    {
        reaction = (Reaction)target;
        Init();
    }

    protected virtual void Init() { }

    public void OnInspectorGUI(int index)
    {
        serializedObject.Update();

        EditorGUILayout.BeginVertical(GUI.skin.box);
        EditorGUI.indentLevel++;

        EditorGUILayout.BeginHorizontal();

        showReaction = EditorGUILayout.Foldout(showReaction, GetFoldoutLabel());

        if (index != 0)
        {
            if (GUILayout.Button("\u25B2", GUILayout.Width(buttonWidth)))
            {
                reactionsProperty.MoveArrayElement(index, index - 1);
                editorParent.RedrawSubEditors();                
            }
        }

        if (index != reactionsProperty.arraySize - 1)
        {
            if (GUILayout.Button("\u25BC", GUILayout.Width(buttonWidth)))
            {
                reactionsProperty.MoveArrayElement(index, index + 1);
                editorParent.RedrawSubEditors();
            }
        }

        if (GUILayout.Button("-", GUILayout.Width(buttonWidth)))
        {
            reactionsProperty.RemoveFromObjectArray(reaction);
        }

        EditorGUILayout.EndHorizontal();

        if (showReaction)
        {
            EditorGUILayout.Space();
            DrawReaction();
            EditorGUILayout.Space();
        }

        EditorGUI.indentLevel--;
        EditorGUILayout.EndVertical();

        serializedObject.ApplyModifiedProperties();
    }

    public static Reaction CreateReaction(Type reactionType)
    {
        return (Reaction)CreateInstance(reactionType);
    }

    protected virtual void DrawReaction()
    {
        DrawDefaultInspector();
    }

    protected abstract string GetFoldoutLabel();
}
