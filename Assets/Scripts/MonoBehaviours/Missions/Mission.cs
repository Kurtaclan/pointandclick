﻿using System;
using UnityEngine;

[Serializable]
public class Mission
{
    [SerializeField]
    private string _missionTitle;

    public string MissionTitle
    {
        get { return _missionTitle; }
        set
        {
            if (_missionTitle == value) return;
            _missionTitle = value;
        }
    }

    [SerializeField]
    private MissionTask[] _missionTasks;

    public MissionTask[] MissionTasks
    {
        get { return _missionTasks; }
        set
        {
            if (_missionTasks == value) return;
            _missionTasks = value;
        }
    }

    [SerializeField]
    private bool _missionComplete;

    public bool MissionComplete
    {
        get { return _missionComplete; }
        set
        {
            if (_missionComplete == value) return;
            _missionComplete = value;
        }
    }
}