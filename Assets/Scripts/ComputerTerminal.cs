﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerTerminal : MonoBehaviour
{
    private MeshRenderer[] ComputerMeshes;
    public Material OnMaterial;
    public Material OffMaterial;
    public bool[] MeshOnOff = new bool[2];
    private bool[] meshOnOffPrev = new bool[2];
    public Interactable[] ComputerInteractables;

    void Awake()
    {
        ComputerMeshes = GetComponentsInChildren<MeshRenderer>();
        ComputerInteractables = GetComponentsInChildren<Interactable>();

        for (int i = 1; i <= ComputerMeshes.Length; i++)
        {
            ChangeTerminalStatus(i, MeshOnOff[i - 1]);
        }
    }

    void Update()
    {
        for (int i = 1; i <= ComputerMeshes.Length; i++)
        {
            if (meshOnOffPrev[i - 1] != MeshOnOff[i - 1])
            {
                ChangeTerminalStatus(i, MeshOnOff[i - 1]);
            }
        }
    }

    void ChangeTerminalStatus(int terminal, bool terminalStatus)
    {
        meshOnOffPrev[terminal - 1] = terminalStatus;
        MeshOnOff[terminal - 1] = terminalStatus;
        ComputerMeshes[terminal - 1].material = terminalStatus ? OnMaterial : OffMaterial;
        ComputerInteractables[terminal - 1].enabled = terminalStatus;
        ComputerMeshes[terminal - 1].GetComponent<BoxCollider>().enabled = terminalStatus;
    }

    public void EnableTerminal(int Terminal)
    {
        ChangeTerminalStatus(Terminal, true);
    }
}
