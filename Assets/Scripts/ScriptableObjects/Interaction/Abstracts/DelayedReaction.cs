﻿using UnityEngine;
using System.Collections;

public abstract class DelayedReaction : Reaction
{
    public float delay;
    internal WaitForSeconds wait;

    public new void Init()
    {
        wait = new WaitForSeconds(delay);

        SpecificInit();
    }

    public new void React(MonoBehaviour monoBehaviour)
    {
        ImmediateReaction();
    }
}
