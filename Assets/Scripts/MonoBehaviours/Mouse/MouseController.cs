﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

public class MouseController : MonoBehaviour
{
    public Texture2D Pointer;
    public Texture2D Arrow;
    public Texture2D Move;
    public Texture2D MoveSmall;
    public Texture2D PointerSmall;
    public LayerMask layerMask;
    private CursorType currentCursor = CursorType.None;
    Ray ray;
    RaycastHit hit;
    private bool animating;
    private static MouseController instance;

    public bool OverrideMouse_Dragging;
    public bool OverrideMouse_MissionUI;
    public bool OverrideMouse_MissionBtn;
    public bool OverrideMouse_InventoryGrid;
    public bool OverrideMouse_Inventory;
    public bool OverrideMouse_Dialog;

    public bool OverrideMouse_Terminal;

    public static MouseController Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<MouseController>();
            }

            return instance;
        }
    }

    void Update()
    {
        if (!Camera.main)
            return;

        if (Time.timeScale == 0 || OverrideMouse_MissionUI || OverrideMouse_MissionBtn || OverrideMouse_InventoryGrid || OverrideMouse_Inventory || OverrideMouse_Dragging || OverrideMouse_Dialog || OverrideMouse_Terminal)
        {
            if (currentCursor != CursorType.Arrow)
            {
                Cursor.SetCursor(Arrow, new Vector2(48, 0), CursorMode.Auto);
                currentCursor = CursorType.Arrow;
            }
            return;
        }

        if (!animating)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100, layerMask))
            {
                switch (hit.collider.tag)
                {
                    case "Floor":
                        if (currentCursor != CursorType.Move)
                        {
                            Cursor.SetCursor(Move, new Vector2(100, 100), CursorMode.Auto);
                            currentCursor = CursorType.Move;
                        }
                        break;

                    case "Interactable":
                        if (currentCursor != CursorType.Pointer)
                        {
                            Cursor.SetCursor(Pointer, new Vector2(66, 0), CursorMode.Auto);
                            currentCursor = CursorType.Pointer;
                        }
                        break;

                    default:
                        if (currentCursor != CursorType.Arrow)
                        {
                            Cursor.SetCursor(Arrow, new Vector2(48, 0), CursorMode.Auto);
                            currentCursor = CursorType.Arrow;
                        }
                        break;
                }
            }
            else
            {
                if (currentCursor != CursorType.Arrow)
                {
                    Cursor.SetCursor(Arrow, new Vector2(48, 0), CursorMode.Auto);
                    currentCursor = CursorType.Arrow;
                }
            }
        }
    }

    public void AnimateMove()
    {
        if (currentCursor == CursorType.Move)
        {
            animating = true;
            Cursor.SetCursor(MoveSmall, new Vector2(100, 100), CursorMode.Auto);

            StartCoroutine(EndAnimation());
        }
        else if (currentCursor == CursorType.Pointer)
        {
            animating = true;
            Cursor.SetCursor(PointerSmall, new Vector2(66, 0), CursorMode.Auto);

            StartCoroutine(EndAnimation());
        }
    }

    private IEnumerator EndAnimation()
    {
        yield return new WaitForSeconds(0.1f);

        if (currentCursor == CursorType.Move)
        {
            Cursor.SetCursor(Move, new Vector2(100, 100), CursorMode.Auto);
        }
        else if (currentCursor == CursorType.Pointer)
        {
            Cursor.SetCursor(Pointer, new Vector2(66, 0), CursorMode.Auto);
        }

        animating = false;
    }

    private enum CursorType
    {
        None,
        Pointer,
        Arrow,
        Move
    }
}